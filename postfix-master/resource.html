<html>

<head>

<title> Postfix Configuration - Resource Controls</title>

<META NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW">
</head>

<body background="obsolete.gif">

<h1><a href="big-picture.html"><img src="small-picture.gif" width="115" height="45"></a> Postfix Configuration - Resource Controls</h1>

<hr>

<p> <strong> Note: this web page is no longer maintained. It exists
only to avoid breaking links in web pages that describe earlier
versions of the Postfix mail system. </strong> </p>

<a href="docs.html">Up one level</a> | <a href="basic.html">Basic
Configuration</a> | <a href="uce.html">UCE Controls</a> | <a
href="rate.html">Rate Controls</a> | Resource Controls | <a
href="rewrite.html">Address Manipulation</a>

<h2> Introduction</h2>

The Postfix system is designed to run within a finite memory budget.
To this end, there are configurable limits on the <i>size</i> of
in-memory objects such as text line fragments, on the <i>number of
instances</i> of such objects, and on the <i>time</i> an operation
may take. In addition, strategies are in place for dealing with
resource exhaustion.  The idea is to keep running under conditions
of stress, without making the problem worse.

<p>

<ul>

<li> <a href="#size">Object size limits</a>

<p>

<li> <a href="#count">Object count limits</a>

<p>

<li> <a href="#time">Time limits</a>

<p>

<li> <a href="#lock">Acquiring exclusive file locks</a>

<p>

<li> <a href="#fan">Error recovery</a>

</ul>

<a name="size"><h2> Object size limits</h2> </a>

The first step towards a fixed memory resource budget is to limit
the size of each in-memory object.  Once the size of in-memory
objects is limited, total memory consumption is limited by limiting
the number of object instances.  Simple, no?

<p>

<dl>

<dt> <b>line_length_limit</b> (default: 2048 bytes)

<dd> How long a line of text can be before it is broken up into
pieces.  All Postfix perimeter programs (<a href="smtpd.8.html">SMTP
server</a>, <a href="smtp.8.html">SMTP client</a>, <a
href="pickup.8.html">local pickup</a> and <a href="local.8.html">local
delivery</a>) enforce this line length limit when reading data from
an untrusted source.  Long lines are reconstructed upon delivery.

<p>

<dt> <b>header_size_limit</b> (default: 102400 bytes)

<dd> How much text may be carried in a multi-line message header.
Header text that does not fit in <b>$header_size_limit</b> bytes
is discarded. This limit is enforced by the <a
href="cleanup.8.html"> cleanup</a> header rewriting code.

<p>

<dt> <b>header_address_token_limit</b> (default: 10240 tokens)

<dd> Limit on the amount of memory and CPU that Postfix will use for
rewriting addresses in message headers. The limit is expressed as
a number of tokens.  Tokens text that do not fit are discarded.
This limit is enforced by the <a href="cleanup.8.html"> cleanup</a>
header rewriting code.

<p>

<dt> <b>extract_recipient_limit</b> (default: 10240 recipients)

<dd> How many recipients Postfix will extract from message headers
before it gives up. This limits the damage that a run-away program
can do with "sendmail -t".

</dl>

<p>

The following parameters restrict the use of file system storage:

<dl>

<dt> <b>message_size_limit</b> (default: 10240000 bytes)

<dd> The maximal size of a Postfix queue file, including envelope
information (sender, recipient, etc.).

<p>

<dt> <b>queue_minfree</b> (default: no restriction)

<dd> How many bytes of free space are needed in the queue file
system.  The <a href="smtpd.8.html">SMTP server</a> declines inbound
mail delivery requests when there is insufficient space (the mail
will be accepted once enough space becomes available).  There is
no default limit; however, it seems like a good idea to require at
least several times <b>$message_size_limit</b> so that the mail
system won't get stuck on a single large message.

<p>

<dt> <b>bounce_size_limit</b> (default: 50000 bytes)

<dd> How much of an undelivered message is sent back to the sender.

</dl>

<a name="count"><h2> Object count limits</h2> </a>

Once the sizes of memory objects have been limited, the next step
to implement Postfix's finite memory budget is to limit the number
of in-memory object instances.

<dl>

<dt> <b>qmgr_message_recipient_limit</b> (default: 20000)

<dd> An upper bound on the number of <a href="qmgr.8.html">queue
manager</a> in-memory recipient address data structures.  This
parameter also controls the number of instances of other in-memory
data structures.  See, for example, the <a
href="rate.html#backoff">delivery rate control</a> documentation.

<p>

<dt> <b>qmgr_message_active_limit</b> (default: 20000)

<dd> An upper limit on the number of messages in the <b>active</b>
queue.  For an introduction to the Postfix queue organization see
the <a href="queuing.html">Postfix overview</a> documentation.

<p>

<dt> <b>duplicate_filter_limit</b> (default: 1000)

<dd> How many recipient addresses the <a href="local.8.html">local
delivery</a> agent and <a href="cleanup.8.html">address cleanup</a>
daemon remember when delivering a message. A recipient address is
ignored when it is found in the remembered list.

</dl>

<a name="time"><h2> Time limits</h2> </a>

External commands are given a finite time for completion. Such
commands are run by the <a href="local.8.html">local</a> delivery
agent when it finds a "|<i>command</i>" destination in an <a
href="aliases.5.html">alias</a> database, <a
href="aliases.5.html">:include:</a> file or <a
href="aliases.5.html">.forward</a> file.  The <a
href="pipe.8.html">pipe</a> mailer implements an alternative way
to pipe mail into external commands.

<dl>

<dt> <b>command_time_limit</b> (default: 1000 seconds)

<dd> How long the <a href="local.8.html">local</a> delivery agent
will wait before aborting an external command.

<p>

<dt> <i>service_name</i><b>_time_limit</b> (default:
<b>$command_time_limit</b>)

<dd> The time limit for delivery to external commands via the
<b>pipe</b> mailer. For <i>service_name</i>, substitute the service
name (the first field in the <b>master.cf </b> file).

</dl>

<a name="lock"><h2> Acquiring exclusive file locks</h2> </a>

Internally, the Postfix programs cooperate in a very disciplined
manner and rarely need to fight for exclusive file access. However,
access conflicts may happen on the outside, for example, when mail
has to be delivered while a user is accessing her mailbox. Postfix
supports two types of file locks:

<ul>

<li>Internal locks, implemented with the <b>fcntl()</b> or
<b>flock()</b> system primitives.

<p>

<li>External locks, implemented as files named <i>file</i><b>.lock</b>.

</ul>

Depending on the host system, Postfix uses one method or both.
The following configuration parameters control how Postfix deals
with file locks:

<dl>

<dt> <b>deliver_lock_attempts</b> (default: 5)

<dd> The number of times to try locking a file before giving up.

<p>

<dt> <b>deliver_lock_delay</b> (default: 1 second)

<dd> How long to wait between attempts to lock a file.

<p>

<dt> <b>stale_lock_time</b> (default: 500)

<dd> How old an external lock file may be before it is forcibly
removed.

</dl>

<a name="fan"><h2> Error recovery</h2> </a>

Under conditions of severe stress, available system resources may
be insufficient to accommodate Postfix's needs. The world may also
seem to fall apart when a Postfix configuration file is broken, or
when a Postfix program is defective.

<p>

The general approach taken in the face of disaster is to terminate
with a fatal run-time error (or with a panic in case of software
problems), and to try again after some time (the <a
href="master.8.html">master</a> daemon will restart processes after
some delay). Each failed attempt is logged; hopefully, someone will
notice the problem and fix it.

<p>

Some recovery strategies were implemented very early during Postfix
development, and haven't been made configurable yet. What follows
is the beginning of a growing list of recovery control parameters:

<dl>

<dt> <b>fork_attempts</b> (default: 5 times)

<dd> The number of times to attempt to create a new process before
giving up.

<p>

<dt> <b>fork_delay</b> (default: 1 second)

<dd> The delay between attempts to create a new process.

<p>

<dt> <b>transport_retry_time</b> (default: 60 seconds)

<dd> The amount of time between queue manager attempts to contact
an apparently defunct Postfix delivery service.

</dl>

<hr>

<a href="docs.html">Up one level</a> | <a href="basic.html">Basic
Configuration</a> | <a href="uce.html">UCE Controls</a> | <a
href="rate.html">Rate Controls</a> | Resource Controls | <a
href="rewrite.html">Address Manipulation</a>

</body>

</html>
